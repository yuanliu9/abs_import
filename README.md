# abs_import

How to install it:
`pip install abs-import`

How to use it:

```python
from abs_import import abs_import
your_module = abs_import('path/to/your/module.py')
```
As a tensorflow object detection api user, you may:
```python
cctr = abs_import('path/to/tensorflow/models/research/object_detection/dataset_tools/create_coco_tf_record.py')
```

Contribution and feature proposal welcome!
